package cz.cvut.od;

import cz.cvut.od.af.AbstractGuiFactory;
import cz.cvut.od.af.Button;
import cz.cvut.od.af.GuiFactoryProvider;
import cz.cvut.od.builder.Car;
import cz.cvut.od.fm.SimpleExportJob;
import cz.cvut.od.fm.TrianglePolygonFactoryMethod;
import cz.cvut.od.singleton.MySingleton;
import cz.cvut.od.singleton.ThreadSafeSingleton;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {

    private static MySingleton mySingleton = MySingleton.getInstance();
    private static MySingleton mySingleton2 = MySingleton.getInstance();

    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchFieldException {

        Class clazz = Class.forName("cz.cvut.od.Person");

        System.out.println("The name of class is " + clazz.getName());

        Constructor constructor = clazz.getConstructor();
        System.out.println("The name of constructor is " + constructor.getName());

        Person person = (Person) constructor.newInstance();

        System.out.println("The public methods of class are : ");
        for (Method method:clazz.getMethods()){
            System.out.println(method.getName());
        }
        System.out.println("Class fields are : ");
        for(Field field: clazz.getDeclaredFields()){
            System.out.println(field.getName());
        }

        Field firstName = clazz.getDeclaredField("firstName");
        Field lastName = clazz.getDeclaredField("lastName");

        firstName.setAccessible(true);
        lastName.setAccessible(true);

        firstName.set(person, "Ondřej");
        lastName.set(person, "Dvorský");

        Method getFullName = clazz.getDeclaredMethod("getFullName");
        System.out.println(getFullName.invoke(person));

        Method getFullNamePrivate = clazz.getDeclaredMethod("getFullNamePrivate");
        getFullNamePrivate.setAccessible(true);

        System.out.println("Invoking private method with result: " + getFullNamePrivate.invoke(person));

        MySingleton mySingleton = MySingleton.getInstance();

        ThreadSafeSingleton threadSafeSingleton = ThreadSafeSingleton.getInstance();
        ThreadSafeSingleton threadSafeSingleton1 = ThreadSafeSingleton.getInstance();

        GuiFactoryProvider.getFactory("win").createButton().paint();

        Car car = new Car.CarBuilder("bmw", "540xi")
                .tireType("sport")
                .windowColor("black")
                .build();
        System.out.println(car);

        car = new Car.CarBuilder("bmw", "540xi")
                .tireType("")
                .build();

        System.out.println(car);
        TrianglePolygonFactoryMethod tpfm = new TrianglePolygonFactoryMethod();
        tpfm.processPolygon();

        SimpleExportJob simpleExportJob = new SimpleExportJob();
        simpleExportJob.schedule();

    }

}
