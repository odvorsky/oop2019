package cz.cvut.od.fm;

public class SimpleExportJob extends AbstractJob {

    @Override
    void runInternal() {
        System.out.println("started exporting");
        try {
            Thread.sleep(2000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("finished exporting");
    }

    @Override
    String cronExpression() {
        return "0 0 0 0 *";
    }
}
