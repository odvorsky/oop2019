package cz.cvut.od.fm;

public abstract class AbstractJob {

    abstract void runInternal();

    abstract String cronExpression();

    public void schedule(){
        System.out.println("scheduing with cron: " + cronExpression());
        System.out.println("running runInternal()");
        runInternal();
    }
}
