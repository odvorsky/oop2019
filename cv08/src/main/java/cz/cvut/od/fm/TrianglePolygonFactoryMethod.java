package cz.cvut.od.fm;

import cz.cvut.od.factory.Polygon;
import cz.cvut.od.factory.Triangle;

public class TrianglePolygonFactoryMethod extends AbstractPolygonFactoryMethod {

    Polygon createPolygon() {
        return new Triangle();
    }
}
