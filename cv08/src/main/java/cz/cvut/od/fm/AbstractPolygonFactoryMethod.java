package cz.cvut.od.fm;

import cz.cvut.od.factory.Polygon;

public abstract class AbstractPolygonFactoryMethod {

    abstract Polygon createPolygon();

    public void processPolygon(){
        Polygon polygon = createPolygon();
        System.out.println(polygon.getType());
    }
}
