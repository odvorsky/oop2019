package cz.cvut.od.af;

public abstract class AbstractGuiFactory {

    public abstract Button createButton();
}
