package cz.cvut.od.af;

public class OSXFactory extends AbstractGuiFactory {

    @Override
    public Button createButton() {
        return new OSXButton();
    }
}
