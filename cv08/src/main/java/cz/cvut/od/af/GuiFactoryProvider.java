package cz.cvut.od.af;

public class GuiFactoryProvider {

    public static AbstractGuiFactory getFactory(String system){
        if("osx".equalsIgnoreCase(system)){
            return new OSXFactory();
        }
        else if("win".equalsIgnoreCase(system)){
            return new WinFactory();
        }
        return null;
    }

}
