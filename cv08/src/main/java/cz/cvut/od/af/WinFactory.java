package cz.cvut.od.af;

public class WinFactory extends AbstractGuiFactory {

    @Override
    public Button createButton() {
        return new WinButton();
    }
}
