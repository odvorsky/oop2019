package cz.cvut.od.builder;

public class Car {
    public String brand;
    public String type;
    public String windowColor;
    public String tireType;

    @Override
    public String toString() {
        return "Car{" +
                "brand='" + brand + '\'' +
                ", type='" + type + '\'' +
                ", windowColor='" + windowColor + '\'' +
                ", tireType='" + tireType + '\'' +
                '}';
    }

    private Car(CarBuilder carBuilder) {
        this.brand = carBuilder.brand;
        this.type = carBuilder.type;
        this.windowColor = carBuilder.windowColor;
        this.tireType = carBuilder.tireType;
    }

    public static class CarBuilder {
        private String brand;
        private String type;
        private String windowColor;
        private String tireType;

        public CarBuilder(String brand, String type) {
            this.brand = brand;
            this.type = type;
        }

        public CarBuilder windowColor(String windowColor){
            this.windowColor = windowColor;
            return this;
        }

        public CarBuilder tireType(String tireType){
            this.tireType = tireType;
            return this;
        }

        public Car build(){
            //pripadna validace zde
            return new Car(this);
        }
    }

    public String getBrand() {
        return brand;
    }

    public String getType() {
        return type;
    }

    public String getWindowColor() {
        return windowColor;
    }

    public String getTireType() {
        return tireType;
    }
}
