package cz.cvut.od.factory;

import java.util.*;
import java.util.stream.Collectors;

public class PolygonFactory {

    private static List<Polygon> polygons = new ArrayList<>(Arrays.asList(new Triangle(), new Square()));
    private static Map<Integer, Polygon> polygonMap = polygons.stream().collect(Collectors.toMap(Polygon::numOfSides, o -> o));

    public static Polygon createPolygon(int numsides){
        return polygonMap.get(numsides);
    }
}
