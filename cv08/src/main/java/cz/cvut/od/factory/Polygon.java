package cz.cvut.od.factory;

public interface Polygon {

    String getType();

    Integer numOfSides();
}
