package cz.cvut.od.factory;

public class Triangle implements Polygon {

    public String getType() {
        return "TRIANGLE";
    }

    public Integer numOfSides() {
        return 3;
    }
}
