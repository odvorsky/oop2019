package cz.cvut.od.factory;

public class Square implements Polygon {

    public String getType() {
        return "SQUARE";
    }

    public Integer numOfSides() {
        return 4;
    }
}
