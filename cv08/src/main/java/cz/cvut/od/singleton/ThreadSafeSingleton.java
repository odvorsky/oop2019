package cz.cvut.od.singleton;

public class ThreadSafeSingleton {

    private ThreadSafeSingleton() {
        System.out.println("singleton initialized");
    }

    private static class SingletonHolder {
        private static final ThreadSafeSingleton instance = new ThreadSafeSingleton();
    }

    public static ThreadSafeSingleton getInstance() {
        return SingletonHolder.instance;
    }


}
