package cz.cvut.od.singleton;

public class MySingleton {

    private static MySingleton instance;

    private MySingleton(){
        System.out.println("created singleton instance");
    }

    public static MySingleton getInstance(){
        if(instance == null){
            instance = new MySingleton();
        }
        return instance;
    }

}
