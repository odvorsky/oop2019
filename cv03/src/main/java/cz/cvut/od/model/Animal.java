package cz.cvut.od.model;

public class Animal {

    public Animal(){
        System.out.println("ANIMAL CREATED");
    }

    public Animal(int numberOfLegs) {
        this.numberOfLegs = numberOfLegs;
    }

    public void bark(){

    }

    protected int numberOfLegs;

    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    public void setNumberOfLegs(int numberOfLegs) {
        this.numberOfLegs = numberOfLegs;
    }


}
