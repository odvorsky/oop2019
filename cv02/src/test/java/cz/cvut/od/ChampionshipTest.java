package cz.cvut.od;

import org.junit.Assert;
import org.junit.Test;

public class ChampionshipTest {

    @Test
    public void generateMatches_teamsRegistered_matchesGeneratedOk(){
        //zakladni assert metody - pro porovnavani
        Assert.assertNotNull(new Object());
        //expected, actual - tzn. vystup z real objectu
        Assert.assertEquals(6, 6);

    }

    //ocekavam, ze metoda skonci vyjimkou, test pak projde
    @Test(expected = IllegalStateException.class)
    public void generateMatches_emptyTest_throwIllegalStateEx(){
        throw new IllegalStateException();

    }

}
