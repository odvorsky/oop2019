package cz.cvut.od.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Championship {

    private String name;
    private Set<Team> teams = new HashSet<>();
    private List<Match> matches = new ArrayList<>();

    public void generateMatches() {
        if(!matches.isEmpty()){
            throw new IllegalStateException("matches were already generated");
        }
        if(teams.isEmpty()){
            throw new IllegalStateException("no teams were registered");
        }
        Set<String> processedTeams = new HashSet<>();
        for(Team home: teams){
            for(Team visitor: teams){
                if(!home.equals(visitor) && !processedTeams.contains(visitor.getName())){
                    matches.add(new Match(home, visitor));
                }
            }
            processedTeams.add(home.getName());

        }
    }

    public void registerTeam(Team team){
        this.teams.add(team);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Team> getTeams() {
        return teams;
    }

    public void setTeams(Set<Team> teams) {
        this.teams = teams;
    }

    public List<Match> getMatches() {
        return matches;
    }

}
