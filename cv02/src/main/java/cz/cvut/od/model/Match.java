package cz.cvut.od.model;

import java.util.Objects;

public class Match {

    private Team home;
    private Team visitor;

    public Match() {
    }

    public Match(Team home, Team visitor) {
        this.home = home;
        this.visitor = visitor;
    }

    public Team getHome() {
        return home;
    }

    public void setHome(Team home) {
        this.home = home;
    }

    public Team getVisitor() {
        return visitor;
    }

    public void setVisitor(Team visitor) {
        this.visitor = visitor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Match match = (Match) o;
        return Objects.equals(home, match.home) &&
                Objects.equals(visitor, match.visitor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(home, visitor);
    }

    @Override
    public String toString() {
        return "Match{" +
                "home=" + home +
                ", visitor=" + visitor +
                '}';
    }
}
