package cz.cvut.od.main;

import cz.cvut.od.model.Auto;
import cz.cvut.od.model.ENCarType;

import java.util.ArrayList;
import java.util.HashSet;

public class Main {

    public static String CONST = "KONSTANTA";
    private String str = "AHOJ";

    public static String staticMethodTest(){
        return "AHOJ AHOJ AHOJ";
    }

    public static void main(String[] args){
        //System.out.println(Auto.createSUV("BMW", "černá", 2019));

        System.out.println(Main.CONST);
        System.out.println(Main.staticMethodTest());

        Auto auto = new Auto();
        auto.setZnacka("BMW");

        Auto auto2 = new Auto();
        auto2.setZnacka("AAA");

        int[] pole = {1,2,3,4};

        new ArrayList<>();
        new HashSet<>();

        System.out.println(ENCarType.NAKLADNI.getPreklad());


    }

}
