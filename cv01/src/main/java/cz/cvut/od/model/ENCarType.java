package cz.cvut.od.model;

public enum ENCarType {
    OSOBNI("osobní"),
    NAKLADNI("nákladní"),
    JINE("Jiné");

    private String preklad;

    ENCarType(String preklad){
        this.preklad = preklad;
    }

    public String getPreklad() {
        return preklad;
    }
}
