package cz.cvut.od.model;

public class Motor {

    private String palivo;
    private double objem;

    public Motor(String palivo){
        this.palivo = palivo;
    }

    public Motor(String palivo, double objem){
        this(palivo);
        this.objem = objem;

    }

    public Motor(){
    }


    public String getPalivo() {
        return palivo;
    }

    public void setPalivo(String palivo) {
        this.palivo = palivo;
    }

    public double getObjem() {
        return objem;
    }

    public void setObjem(Integer objem) {
        this.objem = objem;
    }

    @Override
    public String toString() {
        return "Motor[objem: " + objem
                + ", palivo: " + palivo + "]";
    }
}
