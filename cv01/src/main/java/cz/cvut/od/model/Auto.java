package cz.cvut.od.model;


import java.util.Objects;

public class Auto {


    private Motor motor;
    private  String znacka;
    private String barva;
    private int rokVyroby;


    public String getZnacka() {
        return znacka;
    }

    public void setZnacka(String znacka) {
        this.znacka = znacka;
    }

    public String getBarva() {
        return barva;
    }

    public void setBarva(String barva) {
        this.barva = barva;
    }

    public int getRokVyroby() {
        return rokVyroby;
    }

    public void setRokVyroby(int rokVyroby) {
        this.rokVyroby = rokVyroby;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Auto auto = (Auto) o;
        return Objects.equals(znacka, auto.znacka);

    }

    @Override
    public int hashCode() {
        return Objects.hash(znacka);
    }

    @Override
    public String toString() {
        return "Auto{" +
                "motor=" + motor +
                ", znacka='" + znacka + '\'' +
                ", barva='" + barva + '\'' +
                ", rokVyroby=" + rokVyroby +
                '}';
    }
}
