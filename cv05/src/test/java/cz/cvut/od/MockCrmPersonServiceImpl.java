package cz.cvut.od;

public class MockCrmPersonServiceImpl implements CrmPersonService {

    public String getFullName(String login) {
        if(login == null){
            throw new IllegalArgumentException("login cannot be null");
        }

        return "Adam Kučera";
    }
}
