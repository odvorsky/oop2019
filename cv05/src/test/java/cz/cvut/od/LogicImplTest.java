package cz.cvut.od;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class LogicImplTest {

    @Test
    public void enhanceFullName_validLogin_returnFullnameLoginConcatenated(){
        LogicImpl logic = new LogicImpl();
        CrmPersonService mock = Mockito.mock(CrmPersonService.class);
        logic.setCrmPersonService(mock);

        Mockito.when(mock.getFullName("dvorson1")).thenReturn("Ondřej Dvorský");
        Mockito.when(mock.getFullName("blabla")).thenReturn("Blabla Blabla");

        Assert.assertEquals("Ondřej Dvorský (dvorson1)", logic.enhanceFullName("dvorson1"));
        Assert.assertEquals("Blabla Blabla (blabla)", logic.enhanceFullName("blabla"));

        logic.setCrmPersonService(new MockCrmPersonServiceImpl());
        Assert.assertEquals("Adam Kučera (hehe)", logic.enhanceFullName("hehe"));
    }


}
