package cz.cvut.od;

public interface CrmPersonService {

    String getFullName(String login);
}
