package cz.cvut.od;

import java.util.Arrays;
import java.util.List;

public class Main {


    public static void main(String[] args){
        //asList je taky genericka (aby tam nekdo neplnil rozdilne typy
        List<String> strings = Arrays.asList("AHOJ", "NE", "DOTNOT");


        //ekvivalentni zapisy
        System.out.println(test(strings, s -> s.contains("A")));

        System.out.println(test(strings, new APredicateImpl()));

        System.out.println(test(strings, new OwnPredicate<String>() {
            @Override
            public boolean test(String s) {
                return s.contains("A");
            }
        }));

        System.out.println(sum(Arrays.asList(1, 2, 3)));

        System.out.println(sum(Arrays.asList(1.0, 2.0, 5.0)));
    }

    public static <T extends Number> double sum2(List<T> list){
        double result = 0.0d;
        //TODO
        return result;
    }

    public static double sum(List<? extends Number> list){
        double result = 0.0d;
        //TODO
        return result;
    }

    //vyhledavani v listu na zaklade predikatu
    public static <E> E test(List<E> list, OwnPredicate<E> unaryOperator){
        //TODO
        return null;
    }


}
