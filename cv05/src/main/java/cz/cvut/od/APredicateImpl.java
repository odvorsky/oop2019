package cz.cvut.od;

public class APredicateImpl implements OwnPredicate<String>{

    @Override
    public boolean test(String s) {
        return s.contains("A");
    }
}
