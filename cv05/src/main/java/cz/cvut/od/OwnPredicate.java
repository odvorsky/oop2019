package cz.cvut.od;

public interface OwnPredicate<T> {

    boolean test(T t);
}
