package cz.cvut.od;

public class LogicImpl {

    private CrmPersonService crmPersonService;

    public String enhanceFullName(String login){
        return crmPersonService.getFullName(login) + " (" + login + ")";
    }

    public CrmPersonService getCrmPersonService() {
        return crmPersonService;
    }

    public void setCrmPersonService(CrmPersonService crmPersonService) {
        this.crmPersonService = crmPersonService;
    }
}
