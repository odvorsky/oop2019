package cz.cvut.od.observer;

public interface PostObserver {

    void notifyPostAdded(String post);
}
