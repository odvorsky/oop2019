package cz.cvut.od.observer;

public interface PostObservable {

    void subscribe(Profile profile);

    void unsubscribe(Profile profile);
}
