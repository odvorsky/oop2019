package cz.cvut.od.chain;

public class LoggingHandler extends AbstractHandler<String>{

    @Override
    public void handle(String request, String level) {
        handleNext(request, level);
    }
}
