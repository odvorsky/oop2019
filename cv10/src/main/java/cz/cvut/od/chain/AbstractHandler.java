package cz.cvut.od.chain;

public abstract class AbstractHandler<T> implements Handler<T> {

    private Handler<T> next;

    public void registerNext(Handler<T> next) {
        this.next = next;
    }

    protected void handleNext(T request, String level) {
        if (this.next != null) {
            this.next.handle(request, level);
        }
    }

}
