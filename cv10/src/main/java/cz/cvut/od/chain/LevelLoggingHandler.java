package cz.cvut.od.chain;

public class LevelLoggingHandler extends AbstractHandler<String> {

    private String level;

    public LevelLoggingHandler(String level){
        this.level = level;
    }

    @Override
    public void handle(String request, String level) {
        if(this.level.equalsIgnoreCase(level)){
            System.out.println(this.level + ": " + request);
        }else {
            handleNext(request, level);
        }
    }
}
