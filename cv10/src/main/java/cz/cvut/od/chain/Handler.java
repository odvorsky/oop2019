package cz.cvut.od.chain;

public interface Handler<T> {

    void handle(T request, String level);
}
