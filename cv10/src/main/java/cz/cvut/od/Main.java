package cz.cvut.od;

import cz.cvut.od.chain.LevelLoggingHandler;
import cz.cvut.od.chain.LoggingHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class Main {

    private static final Logger log = LoggerFactory.getLogger(Main.class);

    public static final void main(String[] args) throws IOException {
        /*Profile jagr = new Profile("Jarda Jágr");

        Profile ondra = new Profile("Ondřej Antoš");
        jagr.subscribe(ondra);

        Profile katka = new Profile("Katka Nováková");
        jagr.subscribe(katka);

        jagr.addPost("Ahoj hokejová rodino");
        log.info("Ondruv feed: {}", String.join(",", ondra.getFeed()));
        log.info("Katky feed: {}", String.join(",", katka.getFeed()));


        StringBuilder builder = new StringBuilder("NAPSANE PRIKAZY");
        CopyTextCommand copyTextCommand = new CopyTextCommand(builder, " AHOJ");
        copyTextCommand.execute();

        log.info("obsah editoru: {}", builder.toString());
        copyTextCommand.undo();
        log.info("obsah editoru po undo: {}", builder.toString());


        Order order = new Order();
        order.setTotalAmmount(new BigDecimal("1000.0"));
        order.setPaymentStrategy(new CreditCardPaymentStrategy());

        order.pay();*/

        LoggingHandler loggingHandler = new LoggingHandler();
        LevelLoggingHandler info = new LevelLoggingHandler("info");
        LevelLoggingHandler error = new LevelLoggingHandler("error");

        loggingHandler.registerNext(info);
        info.registerNext(error);

        loggingHandler.handle("AHOJ", "error");

    }

}
