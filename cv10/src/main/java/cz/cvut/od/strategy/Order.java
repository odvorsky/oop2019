package cz.cvut.od.strategy;

import java.io.IOException;
import java.math.BigDecimal;

public class Order {

    private BigDecimal totalAmmount;
    private PaymentStrategy paymentStrategy;

    public void pay() throws IOException {
        paymentStrategy.retrievePaymentInfo();
        paymentStrategy.pay(totalAmmount);
    }

    public void setTotalAmmount(BigDecimal totalAmmount){
        this.totalAmmount = totalAmmount;
    }

    public void setPaymentStrategy(PaymentStrategy paymentStrategy) {
        this.paymentStrategy = paymentStrategy;
    }
}
