package cz.cvut.od.strategy;

import java.io.IOException;
import java.math.BigDecimal;

public interface PaymentStrategy {

    void pay(BigDecimal ammount);

    void retrievePaymentInfo() throws IOException;
}
