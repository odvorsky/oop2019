package cz.cvut.od.strategy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;

public class CreditCardPaymentStrategy implements PaymentStrategy {

    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    private String cardNumber;
    private String cvv;
    private String expirationDate;

    @Override
    public void pay(BigDecimal ammount) {
        System.out.println(String.format("zaplaceno: %s pres kreditni kartu s cislem: %s", ammount, cardNumber));
    }

    @Override
    public void retrievePaymentInfo() throws IOException {
        System.out.println("Zadejte číslo karty: ");
        this.cardNumber = reader.readLine();

        System.out.println("Zadejte cvv: ");
        this.cvv = reader.readLine();

        System.out.println("Zadejte číslo karty: ");
        this.expirationDate = reader.readLine();
    }
}
