package cz.cvut.od.command;

public interface TextEditorCommand {

    void execute();
    void undo();
}
