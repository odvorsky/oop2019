package cz.cvut.od.command;

public class CopyTextCommand implements TextEditorCommand {

    private StringBuilder builder;
    private String textToCopy;
    private String lastState;

    public CopyTextCommand(StringBuilder stringBuilder, String textToCopy){
        this.builder = stringBuilder;
        this.textToCopy = textToCopy;
    }

    @Override
    public void execute() {
        this.lastState = builder.toString();
        this.builder.append(textToCopy);
    }

    @Override
    public void undo() {
        this.builder.delete(0, this.builder.length());
        this.builder.append(lastState);
    }
}
