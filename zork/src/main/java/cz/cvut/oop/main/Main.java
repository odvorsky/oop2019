package cz.cvut.oop.main;

import cz.cvut.oop.game.GameImpl;
import cz.cvut.oop.ui.CommandLineUi;

public class Main {

    public static void main(String[] args){
        new CommandLineUi(new GameImpl()).start();
    }

}
