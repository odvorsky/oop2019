package cz.cvut.oop.game;

/**
 *  This class represents immutable game data holder, all mutable game data should exist within this class
 *  e.g. room map, finished, inventory, weapons..
 */
public class GameDataImpl implements GameData {

    private Room currentRoom;
    private boolean finished = true;

    /**
     *  Room map registration in constructor
     */
    public GameDataImpl(){
        Room baseRoom = new RoomImpl("jeskyně", "temná jeskyně plná nepřátel");
        baseRoom.registerExit(baseRoom);

        this.currentRoom = baseRoom;
    }

    /**
     *  Sets room, where the user currently resides
     */
    @Override
    public void setCurrentRoom(Room currentRoom) {
        this.currentRoom = currentRoom;
    }

    @Override
    public Room getCurrentRoom() {
        return currentRoom;
    }

    /**
     *  Sets finished flag, indicating game is done/finished
     */
    @Override
    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    /**
     *  Retrieves finished flag -> parent components decides whether to end the game
     *  based on this method
     */
    @Override
    public boolean isFinished() {
        return finished;
    }
}
