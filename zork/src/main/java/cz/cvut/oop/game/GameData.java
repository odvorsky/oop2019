package cz.cvut.oop.game;

public interface GameData {

    boolean isFinished();
    void setFinished(boolean finished);
    Room getCurrentRoom();
    void setCurrentRoom(Room currentRoom);
}
