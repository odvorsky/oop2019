package cz.cvut.oop.command;

/**
 *  Represents Command interface containing method to indentify and execute command
 */
public interface Command {

    String getName();
    String execute(String[] arguments);

}
