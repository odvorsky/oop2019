package cz.cvut.od.fw;

public class Car implements ICar{

    private String type;
    private int numberOfWheels;
    private int topSpeed;

    public Car(String type, int numberOfWheels, int topSpeed) {
        this.type = type;
        this.numberOfWheels = numberOfWheels;
        this.topSpeed = topSpeed;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getNumberOfWheels() {
        return numberOfWheels;
    }

    public void setNumberOfWheels(int numberOfWheels) {
        this.numberOfWheels = numberOfWheels;
    }

    public int getTopSpeed() {
        return topSpeed;
    }

    public void setTopSpeed(int topSpeed) {
        this.topSpeed = topSpeed;
    }
}
