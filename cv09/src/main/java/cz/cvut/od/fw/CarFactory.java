package cz.cvut.od.fw;

import java.util.HashMap;
import java.util.Map;

public class CarFactory {

    private static final Map<String, Car> carMap = new HashMap<>();
    static{
        Car car = new Car("sport-bmw", 4, 250);
        carMap.put(car.getType(), car);
    }

    public static ColoredCar createCar(String type, String color){
        Car car = carMap.get(type);
        return new ColoredCar(car, color);
    }
}
