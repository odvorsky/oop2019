package cz.cvut.od.fw;

public class ColoredCar implements ICar{

    private Car car;
    private String color;

    public ColoredCar(Car car, String color) {
        this.car = car;
        this.color = color;
    }

    @Override
    public String getType() {
        return car.getType();
    }

    @Override
    public int getNumberOfWheels() {
        return car.getNumberOfWheels();
    }

    @Override
    public int getTopSpeed() {
        return car.getTopSpeed();
    }
}
