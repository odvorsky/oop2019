package cz.cvut.od.fw;

public interface ICar {

    String getType();
    int getNumberOfWheels();
    int getTopSpeed();
}
