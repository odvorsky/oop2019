package cz.cvut.od.iterator;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class SimpleArrayIterator<T> implements Iterator<T> {

    private int currentIndex = 0;
    private T[] array;

    public SimpleArrayIterator(T[] array){
        this.array = array;
    }

    @Override
    public boolean hasNext() {
        return currentIndex <= (array.length - 1);
    }

    @Override
    public T next() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }
        return array[currentIndex++];
    }
}
