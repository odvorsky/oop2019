package cz.cvut.od.iterator;

import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

public class MyList<T> implements Iterable<T> {

    private T[] array;

    public MyList(T[] array){
        this.array = array;
    }

    @Override
    public Iterator<T> iterator() {
        return new SimpleArrayIterator<>(array);
    }

    @Override
    public void forEach(Consumer<? super T> action) {

    }

    @Override
    public Spliterator<T> spliterator() {
        return null;
    }
}
