package cz.cvut.od.main;

import cz.cvut.od.iterator.MyList;
import cz.cvut.od.iterator.SimpleArrayIterator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Main {


    public static final void main(String[] args){

        String[] pole = new String[]{"A", "B", "C"};
        MyList<String> myList = new MyList<>(pole);

        SimpleArrayIterator<String> it = new SimpleArrayIterator<>(pole);

        List<String> list = new ArrayList<>(Arrays.asList(pole));

        Iterator<String> iterator = list.iterator();
        while(iterator.hasNext()){
            String item = iterator.next();
            if(item.equalsIgnoreCase("a")){
                iterator.remove();
            }
        }
    }

}
