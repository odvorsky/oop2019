package cz.cvut.od.adapter;

public class PatientDashboard {

    private ClientInfoService clientInfoService;

    public PatientDashboard(ClientInfoService clientInfoService) {
        this.clientInfoService = clientInfoService;
    }

    public void loadData(String rc){
        System.out.println(clientInfoService.getClientInfo(rc));
    }
}
