package cz.cvut.od.adapter;

public interface ClientInfoService {

    String getClientInfo(String rc);
}
