package cz.cvut.od.adapter;

public class ClientInfoServiceAdapter implements ClientInfoService {

    /* pri pripadnem vyhozeni LegacyPatientService budto vytvorime novou implementaci ClientInfoService, nebo upravime
        primo adapter
    */
    private LegacyPatientService legacyPatientService;

    public ClientInfoServiceAdapter(LegacyPatientService legacyPatientService) {
        this.legacyPatientService = legacyPatientService;
    }

    @Override
    public String getClientInfo(String rc) {
        return legacyPatientService.getFirstName(rc) + " " + legacyPatientService.getLastName(rc);
    }
}
