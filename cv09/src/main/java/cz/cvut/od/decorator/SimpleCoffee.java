package cz.cvut.od.decorator;

public class SimpleCoffee implements Coffee{

    @Override
    public String prepareCofee() {
        return "Káva";
    }
}
