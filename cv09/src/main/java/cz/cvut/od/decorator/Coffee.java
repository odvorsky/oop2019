package cz.cvut.od.decorator;

public interface Coffee {

    String prepareCofee();
}
