package cz.cvut.od.decorator;

public class MilkCoffee implements Coffee{

    private Coffee coffee;

    public MilkCoffee(Coffee coffee) {
        this.coffee = coffee;
    }

    @Override
    public String prepareCofee() {
        String result = this.coffee.prepareCofee();
        return result + " s mlékem";
    }
}
