package cz.cvut.od.proxy;

public class InputOutputLoggingProxy implements Service {

    private Service service;

    public InputOutputLoggingProxy(Service service) {
        this.service = service;
    }

    @Override
    public String call(String input) {
        System.out.println(input);
        String result = service.call(input);
        System.out.println(result);
        return result;
    }
}
