package cz.cvut.od.proxy;

public interface Service {

    String call(String input);

}
